package com.lemoncog.sensorgatherer.sensors;

import android.hardware.SensorEvent;

public class AccelerometerModel extends SensorModel
{
	private float[] mXYZ = new float[3];
	private long mEventTimestamp = -1 ;

	public void parseSensorEvent(SensorEvent event)
	{
		if(event != null)
		{
			setAxisValues(event.values);
			mEventTimestamp = event.timestamp;
		}
	}
	
	public long getEventTimestamp()
	{
		return mEventTimestamp;
	}
	
	public float getStrength()
	{
		return (mXYZ[0] + mXYZ[1] + mXYZ[2]) /3;
	}
	
	public float[] getAxisValues()
	{
		return mXYZ;
	}

	public void setAxisValues(float[] XYZ)
	{
		mXYZ[0] = XYZ[0];
		mXYZ[1] = XYZ[1];
		mXYZ[2] = XYZ[2];
	}

	@Override
	public boolean equals(Object o)
	{
		if(o instanceof AccelerometerModel)
		{
			AccelerometerModel modelB = (AccelerometerModel) o;
			return mXYZ[0] == modelB.getAxisValues()[0] && mXYZ[1] == modelB.getAxisValues()[1] && mXYZ[2] == modelB.getAxisValues()[2];
		}
		
		return false;
	}
}
