package com.lemoncog.sensorgatherer.repo;

import java.util.List;

public abstract class RuleBasedSensorRepository<T> implements SensorRepository<T>
{
	public interface StorageRule<T>
	{
		public boolean isTrue(T data);
	}

	public class ChangesOnlyRule implements StorageRule<T>
	{
		private RuleBasedSensorRepository<T> mSensorRepo;

		public ChangesOnlyRule(RuleBasedSensorRepository<T> sensorRepo)
		{
			mSensorRepo = sensorRepo;
		}

		@Override
		public boolean isTrue(T data)
		{
			T lastData = null;
			
			List<T> storedData = mSensorRepo.getStoredData();
			if (storedData.size() > 0)
			{
				lastData = storedData.get(storedData.size()-1);
			}

			return data == null ? false : !data.equals(lastData);
		}
	}

	private StorageRule<T> mStorageRule;

	public RuleBasedSensorRepository()
	{
		mStorageRule = new ChangesOnlyRule(this);
	}

	public long storeSensorData(T data)
	{
		if (mStorageRule.isTrue(data))
		{
			return addSensorData(data);
		}

		return -1;
	};

	public abstract long addSensorData(T data);
}