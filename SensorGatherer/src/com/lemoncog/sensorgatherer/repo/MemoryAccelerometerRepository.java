package com.lemoncog.sensorgatherer.repo;

import java.util.ArrayList;
import java.util.List;

import com.lemoncog.sensorgatherer.sensors.AccelerometerModel;

public class MemoryAccelerometerRepository extends RuleBasedSensorRepository<AccelerometerModel>
{
	private List<AccelerometerModel> mMemoryStorage;

	// Static to avoid multiple classes causing a clash in IDs
	private static long mUniqueID = 0;

	public MemoryAccelerometerRepository()
	{
		super();

		mMemoryStorage = new ArrayList<AccelerometerModel>();
	}

	@Override
	public List<AccelerometerModel> getStoredData()
	{
		return mMemoryStorage;
	}

	@Override
	public long addSensorData(AccelerometerModel data)
	{
		mUniqueID++;
		data.setID(mUniqueID);

		mMemoryStorage.add(data);

		return mUniqueID;
	}
}
