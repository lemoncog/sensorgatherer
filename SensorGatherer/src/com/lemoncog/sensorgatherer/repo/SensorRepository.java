package com.lemoncog.sensorgatherer.repo;

import java.util.List;

public interface SensorRepository<T> {
	public long storeSensorData(T data);
	public List<T> getStoredData();
}
