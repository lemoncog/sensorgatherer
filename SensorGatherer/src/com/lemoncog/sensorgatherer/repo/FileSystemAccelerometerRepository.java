package com.lemoncog.sensorgatherer.repo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.lemoncog.sensorgatherer.sensors.AccelerometerModel;

public class FileSystemAccelerometerRepository extends RuleBasedSensorRepository<AccelerometerModel>
{
	private static final String TAG = "FileSystemAccelerometerRepository";

	private static final String STORAGE_PATH = "STORAGE_PATH";
	private static final String FILE_SYSTEM_ACCELEROMETER_REPOSITORY = "FileSystemAccelerometerRepository";
	private Context mContext;
	private File mSessionStorageArea;

	public FileSystemAccelerometerRepository(Context aContext)
	{
		super();

		mContext = aContext;

		setupStorageArea();
	}

	public void setupStorageArea()
	{
		// Set up the storage area if we haven't already.
		File lastStorageArea = getStorageLocation();

		if (lastStorageArea == null)
		{
			// Prefer external for bigger size
			if (isExternalStorageWritable() && isExternalStorageReadable())
			{
				mSessionStorageArea = mContext.getExternalFilesDir(null);
			}
		} else
		{
			mSessionStorageArea = lastStorageArea;
		}
	}

	private void saveToFile(String dataJSON, String aFileName)
	{
		File saveFile = new File(mSessionStorageArea, aFileName);

		OutputStream os;
		try
		{
			os = new FileOutputStream(saveFile);

			os.write(dataJSON.getBytes());

			os.flush();

			os.close();

		} catch (FileNotFoundException e)
		{
			Log.e(TAG, "", e);
		} catch (IOException e)
		{
			Log.e(TAG, "", e);
		}
	}
	

	@Override
	public long addSensorData(AccelerometerModel data)
	{
		Gson gson = new Gson();

		String dataJSON = gson.toJson(data);

		saveToFile(dataJSON, "accel-" + new Date().getTime());

		return -1;
	}

	@Override
	public List<AccelerometerModel> getStoredData()
	{
		return readAllFilesInDir(mSessionStorageArea);
	}

	private List<AccelerometerModel> readAllFilesInDir(File storageArea)
	{
		List<AccelerometerModel> storedModels = new ArrayList<AccelerometerModel>();

		File[] childFiles = storageArea.listFiles();

		for (int i = 0; i < childFiles.length; i++)
		{
			storedModels.add(readFromFile(childFiles[i]));
		}
		
		return storedModels;
	}

	private AccelerometerModel readFromFile(File file)
	{
		Gson gson = new Gson();
		JsonReader jsonReader;

		InputStream is;
		InputStreamReader isReader;

		try
		{
			is = new FileInputStream(file);

			isReader = new InputStreamReader(is);
			jsonReader = new JsonReader(isReader);

			return gson.fromJson(jsonReader, AccelerometerModel.class);

		} catch (FileNotFoundException e)
		{
			Log.e(TAG, "", e);
		}

		return null;
	}

	public File getStorageLocation()
	{
		SharedPreferences prefs = mContext.getSharedPreferences(FILE_SYSTEM_ACCELEROMETER_REPOSITORY, Context.MODE_PRIVATE);

		String storagePath = prefs.getString(STORAGE_PATH, null);

		if (storagePath != null)
		{
			return new File(storagePath);
		}

		return null;
	}

	public void setStorageLocation(File aFile)
	{
		SharedPreferences prefs = mContext.getSharedPreferences(FILE_SYSTEM_ACCELEROMETER_REPOSITORY, Context.MODE_PRIVATE);

		Editor editor = prefs.edit();

		editor.putString(STORAGE_PATH, aFile.getAbsolutePath());

		editor.commit();
	}

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable()
	{
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
		{
			return true;
		}
		return false;
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable()
	{
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
		{
			return true;
		}
		return false;
	}
}
