package com.lemoncog;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import com.lemoncog.sensorgatherer.R;

@RunWith(RobolectricTestRunner.class)
public class MyActivityTest {

    @Test
    public void shouldHaveHappySmiles() throws Exception {
        String hello = Robolectric.getShadowApplication().getResources().getString(R.string.hello_world);
        assertThat(hello, equalTo("Hello world!"));
    }
}