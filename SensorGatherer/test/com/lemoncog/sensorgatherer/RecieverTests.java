package com.lemoncog.sensorgatherer;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;

import android.content.Context;

import com.lemoncog.robolectric.MockitoEletricRunner;
import com.lemoncog.sensorgatherer.sensors.AccelerometerModel;
import com.lemoncog.sensorgatherer.sensors.AccelerometerReciever;
import com.lemoncog.sensorgatherer.sensors.SensorMonitor;

@RunWith(MockitoEletricRunner.class)
public class RecieverTests
{
	private Context APP_CONTEXT = Robolectric.application.getApplicationContext();

	@Test
	public void recieverStartsOff()
	{
		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(APP_CONTEXT);

		// We didn't call startTracking so shouldn't have any data in memory.
		Assert.assertFalse(accelemoterHandler.isTracking());
	}

	@Test
	public void recieverStarts()
	{
		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(APP_CONTEXT);

		accelemoterHandler.startTracking();

		Assert.assertTrue(accelemoterHandler.isTracking());
	}

	@Test
	public void receiverRecievesAndSendsDataStarted()
	{
		// SensorEvent event = mock(SensorEvent.class);
		SensorMonitor<AccelerometerModel> accelMonitor = mock(SensorMonitor.class);

		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(APP_CONTEXT);
		accelemoterHandler.addMonitor(accelMonitor);

		accelemoterHandler.startTracking();
		accelemoterHandler.onSensorChanged(null);

		verify(accelMonitor).newDataAcquired(any(AccelerometerModel.class));
	}


//	public void testStoreData()
//	{
//		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());
//		accelemoterHandler.startTracking();
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//
//		SensorRepository<AccelerometerModel> localMemoryRepo = new MemoryAccelerometerRepository();
//
//		long[] generatedIDs = localMemoryRepo.storeSensorDataBatch(accelemoterHandler.getDataGathered());
//
//		Assert.assertTrue(generatedIDs.length == 4);
//
//		List<AccelerometerModel> storedData = localMemoryRepo.getStoredData();
//
//		Assert.assertTrue(storedData.size() == 4);
//
//		for (int i = 0; i < storedData.size(); i++)
//		{
//			int sameCount = 0;
//
//			for (int d = 0; d < storedData.size(); d++)
//			{
//				if (generatedIDs[i] == generatedIDs[d])
//				{
//					sameCount++;
//				}
//
//				if (sameCount > 1)
//				{
//					Assert.fail("Duplicate data found");
//				}
//			}
//		}
//
//		for (int i = 0; i < storedData.size(); i++)
//		{
//			int sameCount = 0;
//
//			for (int d = 0; d < storedData.size(); d++)
//			{
//				if (storedData.get(i) == storedData.get(d) || storedData.get(i).getID() == storedData.get(d).getID())
//				{
//					sameCount++;
//				}
//
//				if (sameCount > 1)
//				{
//					Assert.fail("Duplicate data found");
//				}
//			}
//		}
//	}

	public static AccelerometerModel buildGoodData()
	{
		AccelerometerModel model = new AccelerometerModel();

		model.setAxisValues(new float[]
		{ 1, 1, 1 });

		return model;
	}
}
