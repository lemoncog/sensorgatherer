package com.lemoncog.sensorgatherer;

import org.hamcrest.core.AnyOf;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.Any;

import static org.mockito.Mockito.*;
import org.mockito.runners;

import junit.framework.Assert;
import android.hardware.SensorEvent;
import android.test.AndroidTestCase;

import com.lemoncog.sensorgatherer.sensors.AccelerometerModel;
import com.lemoncog.sensorgatherer.sensors.AccelerometerReciever;
import com.lemoncog.sensorgatherer.sensors.SensorMonitor;

@RunWith(MockitoJUnitRunner.class)
public class RecieverTests extends AndroidTestCase
{
	public void testRecieverStartsOff()
	{
		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());

		// We didn't call startTracking so shouldn't have any data in memory.
		Assert.assertFalse(accelemoterHandler.isTracking());
	}
	
	public void testRecieverStarts()
	{
		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());

		accelemoterHandler.startTracking();
		
		Assert.assertTrue(accelemoterHandler.isTracking());
	}
	
	@Mock
	public SensorMonitor<AccelerometerModel> mAccelMonitor;
	
	public void testReceiverRecievesAndSendsDataStarted()
	{
		//SensorEvent event = mock(SensorEvent.class);
			
		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());
		accelemoterHandler.addMonitor(mAccelMonitor);

		accelemoterHandler.startTracking();
		accelemoterHandler.onSensorChanged(null);
		
		verify(mAccelMonitor).newDataAcquired(any(AccelerometerModel.class));
	}

//	public void testHandleSensorData()
//	{
//		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());
//
//		accelemoterHandler.startTracking();
//
//		accelemoterHandler.handleData(buildGoodData());
//
//		// Assert we have managed to hold some data in memory.
//		Assert.assertNotNull(accelemoterHandler.getDataGathered());
//		Assert.assertTrue(accelemoterHandler.getDataGathered().size() > 0);
//	}
//
//	public void testStoreData()
//	{
//		AccelerometerReciever accelemoterHandler = new AccelerometerReciever(getContext());
//		accelemoterHandler.startTracking();
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//		accelemoterHandler.handleData(buildGoodData());
//
//		SensorRepository<AccelerometerModel> localMemoryRepo = new MemoryAccelerometerRepository();
//
//		long[] generatedIDs = localMemoryRepo.storeSensorDataBatch(accelemoterHandler.getDataGathered());
//
//		Assert.assertTrue(generatedIDs.length == 4);
//
//		List<AccelerometerModel> storedData = localMemoryRepo.getStoredData();
//
//		Assert.assertTrue(storedData.size() == 4);
//
//		for (int i = 0; i < storedData.size(); i++)
//		{
//			int sameCount = 0;
//
//			for (int d = 0; d < storedData.size(); d++)
//			{
//				if (generatedIDs[i] == generatedIDs[d])
//				{
//					sameCount++;
//				}
//
//				if (sameCount > 1)
//				{
//					Assert.fail("Duplicate data found");
//				}
//			}
//		}
//
//		for (int i = 0; i < storedData.size(); i++)
//		{
//			int sameCount = 0;
//
//			for (int d = 0; d < storedData.size(); d++)
//			{
//				if (storedData.get(i) == storedData.get(d) || storedData.get(i).getID() == storedData.get(d).getID())
//				{
//					sameCount++;
//				}
//
//				if (sameCount > 1)
//				{
//					Assert.fail("Duplicate data found");
//				}
//			}
//		}
//	}

	public static AccelerometerModel buildGoodData()
	{
		AccelerometerModel model = new AccelerometerModel();

		model.setAxisValues(new float[]
		{ 1, 1, 1 });

		return model;
	}
}
